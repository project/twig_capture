<?php

namespace Drupal\twig_capture;

use Twig\Compiler;
use Twig\Node\Node;

/**
 * Omit the debug info: it contains the line number and causes a mismatch.
 */
class TwigCaptureCompiler extends Compiler {

  /**
   * {@inheritDoc}
   */
  public function addDebugInfo(Node $node) {
    // Intentionally blank to avoid line miss-matches.
    return $this;
  }

}
