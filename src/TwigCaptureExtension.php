<?php

namespace Drupal\twig_capture;

use Drupal\Core\Template\TwigEnvironment;
use Twig\Extension\AbstractExtension;

/**
 * Twig extension that captures render checks and optimizes them.
 */
class TwigCaptureExtension extends AbstractExtension {

  public function __construct(protected TwigEnvironment $environment) {
  }

  /**
   * {@inheritDoc}
   */
  public function getNodeVisitors() {
    return [
      new TwigCaptureNodeVisitor($this->environment),
    ];
  }

}
